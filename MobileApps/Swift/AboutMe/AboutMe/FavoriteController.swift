//
//  FavoriteViewController.swift
//  AboutMe
//
//  Created by Andrew Reyes on 10/17/17.
//  Copyright © 2017 Andrew Reyes. All rights reserved.
//

import Foundation

import UIKit

class FavoriteController: UITableViewController {
    
    var favoriteStore = FavoriteStore()
    
    override func viewDidLoad() {

        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let insets = UIEdgeInsets(top: statusBarHeight, left: 0, bottom: 0, right: 0)
        
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
    }
    

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return favoriteStore.allFavorites.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        let item = favoriteStore.allFavorites[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = "\(item.criticRating)"
        
        return cell
    }
    
}
