//
//  FavoriteStore.swift
//  AboutMe
//
//  Created by Andrew Reyes on 10/17/17.
//  Copyright © 2017 Andrew Reyes. All rights reserved.
//

import UIKit

class FavoriteStore {
    
    var allFavorites = [Favorite]()
    
    init() {
        for _ in 0..<5 {
            createRandomItem()
        }
    }
    
    func createRandomItem() {
        let newFavorite = Favorite(random: true)
        allFavorites.append(newFavorite)
    }
    
}
