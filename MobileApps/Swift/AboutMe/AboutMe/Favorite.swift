//
//  Favorite.swift
//  AboutMe
//
//  Created by Andrew Reyes on 10/17/17.
//  Copyright © 2017 Andrew Reyes. All rights reserved.
//

import UIKit


class Favorite: NSObject {
    
    var name: String
    //var serialNumber: String?
    var criticRating: Float
    //let dateCreated: Date
    
    init(name: String, criticRating: Float) {

        self.name = name
        //self.serialNumber = serialNumber
        self.criticRating = criticRating
        //self.dateCreated = Date()
        
        super.init()
    }
    
    
    convenience init(random: Bool = false) {
        if random {
            let randomName = Favorite.generateRandomName()
            //let randomSerialNumber = UUID().uuidString.components(separatedBy: "-").first!
            let randomValue = Float(arc4random_uniform(100))/10.0

            self.init(name: randomName, criticRating: randomValue)
        } else {

            self.init(name: "", criticRating: 0.0)
        }
    }
    

    static func generateRandomName() -> String {
        let movieTitles = ["FlashPoint", "Dark Knight", "Losers", "Terminator", "Rocky", "Central Intelligence", "Despicable Me", "Superman", "Justice League", "DeadPool"]

 
        let index = arc4random_uniform(UInt32(movieTitles.count))
        let randomMovieTitles = movieTitles[Int(index)]
        
        
        return "\(randomMovieTitles)"
    }
    
}
