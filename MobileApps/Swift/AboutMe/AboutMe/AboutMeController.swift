//
//  AboutMeController.swift
//  AboutMe
//
//  Created by Andrew Reyes on 10/14/17.
//  Copyright © 2017 Andrew Reyes. All rights reserved.
//

import MapKit
import UIKit


class AboutMeController: UIViewController, UITextFieldDelegate, MKMapViewDelegate{
    
    @IBOutlet weak var guessNumber: UITextField!
    @IBOutlet weak var guessLabel: UILabel!
    
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var birthdayButton: UIButton!

    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
 
    var buttonClicked = false 

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.guessNumber.keyboardType = .numberPad
        guessNumber.text = "0"
        guessLabel.text = "0 is less than the number of pets that I own! I have a dog named Amos."
       
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 29.9552, longitude: -90.07841)
        mapView.addAnnotation(annotation)
        
        let initialLocation = CLLocation(latitude: 29.9552, longitude: -90.07841)
        
        let regionRadius: CLLocationDistance = 1000
        
        func centerMapOnLocation(location: CLLocation) {
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
            mapView.setRegion(coordinateRegion, animated: true)
        }

        centerMapOnLocation(location: initialLocation)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.colorLabel.backgroundColor = UIColor.white
        self.colorLabel.layer.borderColor = UIColor.black.cgColor
        self.colorLabel.layer.borderWidth = 1.0
        self.colorLabel.layer.cornerRadius = self.colorButton.frame.height/5
        
        self.birthdayButton.backgroundColor = UIColor.white
        self.birthdayButton.layer.borderColor = UIColor.black.cgColor
        self.birthdayButton.layer.borderWidth = 1.0
        self.birthdayButton.layer.cornerRadius = self.birthdayButton.frame.height/5
        
        self.colorLabel.text = "Click here to see my favorite color"
        self.birthdayLabel.text = "My Birthday"
        
    }
    
    
    @IBAction func animateColorButton(_ sender: UIButton) {
        let color = UIColor(red: 8, green: 40, blue: 82)
    
        if buttonClicked == true {
        
            UIView.animate(withDuration: 1.5, animations:{

            self.colorLabel.backgroundColor = .white
            self.colorLabel.layer.borderWidth = 1.0
            self.colorLabel.text = "Click here to see my favorite color"
            self.colorLabel.textColor = .black
            self.colorLabel.layer.cornerRadius = self.colorButton.frame.height/5
            
            self.buttonClicked = false
        }, completion: nil )
        
    }else {
        UIView.animate(withDuration: 1.5, animations:{

            self.colorLabel.backgroundColor = color
            self.colorLabel.text = "Hide favorite color"
            self.colorLabel.textColor = .white
            self.colorButton.layer.borderWidth = 1.0
            self.colorButton.layer.cornerRadius = self.colorButton.frame.height/5

            self.buttonClicked = true
            
            }, completion: nil )
        }
     }
    
    
    @IBAction func animateBirthdayButton(_ sender: UIButton) {

        UIView.animate(withDuration: 1, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.birthdayLabel.alpha = 1
            self.birthdayLabel.text = "August 19, 1991"
            
        },completion:{ (finished: Bool) -> Void in
            
        UIView.animate(withDuration: 1, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.birthdayLabel.alpha = 0
            
            }, completion: nil)
        })
    }

    
    @IBAction func guessQuestion(_ sender: UITextField) {
        var guess = 0
        guess = Int(guessNumber.text ?? "") ?? 0

            if(guess < 1){
                guessLabel.text =  "\(guess) is less than the number of pets that I own!"
            }
        else if (guess > 1)
        {
            guessLabel.text = "\(guess) is more than the number of pets that I own!"
        
        }else {
            guessLabel.text = "\(guess) is the number of pets that I own!"
        }
    }
    
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        guessNumber.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
 
        let allowedCharacters = CharacterSet(charactersIn: "0123456789")
        let replacementStringCharacterSet = CharacterSet(charactersIn: string)
        
        if !replacementStringCharacterSet.isSubset(of: allowedCharacters) {
            return false
        }else {
            return true
        }

    }
    
    
    //I like this feature when using the Mac keyboard. Was not part of the assignment.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guessNumber.resignFirstResponder()
        return true
    }
}


//UICOLOR Class
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}
