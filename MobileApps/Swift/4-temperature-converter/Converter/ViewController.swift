//
//  ViewController.swift
//  Converter
//
//  Created by Brittany Allesandro on 9/6/17.
//  Copyright © 2017 Brittany Allesandro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myFirstFrame = CGRect(x: 20, y: 50, width: 100, height: 200)
        let myFirstView = UIView(frame: myFirstFrame)
        myFirstView.backgroundColor = UIColor.blue
        view.addSubview(myFirstView)
        
        let mySecondFrame = CGRect(x: 10, y: 10, width: 25, height: 25)
        let mySecondView = UIView(frame: mySecondFrame)
        mySecondView.backgroundColor = UIColor.red
        myFirstView.addSubview(mySecondView)
    }
    

}



