//
//  ConverterController.swift
//  Converter
//
//  Created by Brittany Allesandro on 9/6/17.
//  Copyright © 2017 Brittany Allesandro. All rights reserved.
//

import UIKit

class ConverterController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var fahrenheitText: UITextField!
    @IBOutlet weak var celsiusText: UILabel!
    
    var fahrenheitValue: Measurement<UnitTemperature>? {
        didSet {
            updateCelsiusLabel()
        }
    }
    var celsiusValue: Measurement<UnitTemperature>? {
        if let temp = fahrenheitValue {
            return temp.converted(to: .celsius)
        } else {
            return nil
        }
    }
    
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    func updateCelsiusLabel() {
        if let celsiusValue = celsiusValue {
            let number = NSNumber(value: celsiusValue.value)
            let string = numberFormatter.string(from: number)!
            celsiusText.text = "\(string)"
        } else {
            celsiusText.text = "???"
        }
    }
    
    @IBAction func changedTemperature(_ sender: UITextField) {
        if let text = sender.text, let value = Double(text) {
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit)
        } else {
            fahrenheitValue = nil
        }
    }
    
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        fahrenheitText.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let allowedCharacters = CharacterSet(charactersIn: "0123456789.")
        let replacementStringCharacterSet = CharacterSet(charactersIn: string)
        
        if !replacementStringCharacterSet.isSubset(of: allowedCharacters) {
            return false
        }
        
        let existingDecimalRange = fahrenheitText.text?.range(of: ".")
        let replacementDecimalRange = string.range(of: ".")
        
        if existingDecimalRange != nil && replacementDecimalRange != nil {
            return false
        } else {
            return true
        }
        

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fahrenheitText.resignFirstResponder()
        return true
    }

    
}

